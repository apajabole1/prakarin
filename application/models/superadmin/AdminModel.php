<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminModel extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    return $this->db->get('tb_admin')->result();
  }

  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($user_id){
    $this->db->where('user_id', $user_id);
    return $this->db->get('tb_admin')->row();
  }

  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya

    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data siswa saja
    if($mode == "save")

    $this->form_validation->set_rules('input_user_email', 'Email', 'required|max_length[50]');
    $this->form_validation->set_rules('input_user_password', 'Password', 'required|max_length[100]');
    $this->form_validation->set_rules('input_user_verifikasi', 'User Verifikasi', 'required');

    if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
  }

  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
    $data = array(
      "user_email" => $this->input->post('input_user_email'),
      "user_password" => $this->input->post('input_user_password'),
      "user_verifikasi" => $this->input->post('input_user_verifikasi')
    );

    $this->db->insert('tb_admin', $data); // Untuk mengeksekusi perintah insert data
  }

  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($user_id){
    $data = array(
      "user_email" => $this->input->post('input_user_email'),
      "user_password" => $this->input->post('input_user_password'),
      "user_verifikasi" => $this->input->post('input_user_verifikasi')
    );

    $this->db->where('user_id', $user_id);
    $this->db->update('tb_admin', $data); // Untuk mengeksekusi perintah update data
  }

  // Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
  public function delete($user_id){
    $this->db->where('user_id', $user_id);
    $this->db->delete('tb_admin'); // Untuk mengeksekusi perintah delete data
  }
}
 