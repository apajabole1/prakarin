<?php

class LoginModel extends CI_Model
{

  public function login($user_email,$user_password,$user_level)
  {
     if ($user_level == "Superadmin") {
       $this->db->select('*');
       $this->db->from('tb_superadmin');
       $this->db->where('user_email',$user_email);
       $this->db->where('user_password',$user_password);
       $this->db->limit(1);

       $query = $this->db->get();

      if ($query->num_rows()==1) {
        return $query->result();
      } else {
        return false;
      }

    } elseif ($user_level == "Admin") {
        $this->db->select('*');
        $this->db->from('tb_admin');
        $this->db->where('user_email',$user_email);
        $this->db->where('user_password',$user_password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows()==1) {
          return $query->result();
        } else {
          return false;
        }
	  } elseif ($user_level == "Siswa" || $user_level == "Dosen" || $user_level == "Kajur" || $user_level == "Kaprodi" ) {
        if ($user_level == "Siswa") {
          $this->db->select('*');
          $this->db->from('tb_user');
          $this->db->join('tb_siswa','tb_siswa.nis=tb_user.nis');
          $this->db->where('user_email',$user_email);
          $this->db->where('user_password',$user_password);
          $this->db->limit(1);

        } elseif ($user_level == "Dosen" || $user_level == "Kajur" || $user_level == "Kaprodi" ) {
          $this->db->select('*');
          $this->db->from('tb_user');
          $this->db->join('tb_dosen','tb_dosen.id_dosen=tb_user.id_dosen');
          $this->db->where('user_email',$user_email);
          $this->db->where('user_password',$user_password);
          $this->db->limit(1);
        }

        $querycek = $this->db->get();

        if ($querycek->num_rows()==1) {
            return $querycek->result();
        } else {
            $this->db->select('*');
            $this->db->from('tb_user');
            // $this->db->join('tb_dosen','tb_dosen.id_dosen=tb_user.id_dosen');
            $this->db->where('user_email',$user_email);
            $this->db->where('user_password',$user_password);
            $this->db->limit(1);

            $query = $this->db->get();

            if ($query->num_rows()==1) {
              return $query->result();
            } else {
              return false;
            }
        }
    }

	}
}
