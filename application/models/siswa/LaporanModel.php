<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class LaporanModel extends CI_Model{
	
	public function validation($mode) {
	
	$this->load->library('form_validation'); 
    if($mode == "save")
	
	$this->form_validation->set_rules('input_id_laporan', 'ID Laporan', 'required|max_length[25]');
    $this->form_validation->set_rules('input_nis', 'NIS', 'required|max_length[25]');
    $this->form_validation->set_rules('input_pembimbing', 'Pembimbing', 'required|max_length[50]');
    $this->form_validation->set_rules('laporan', 'Laporan', 'required|max_length[25]');	
	
	 if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
	}
	
	public function save(){
		$data = array(
		"id_laporan" => $this->input->post('input_id_laporan'),
		"nis" => $this->input->post('input_nis'),
		"id_pembimbing" => $this->input->post('input_id_pembimbing'),
		"laporan" => $this->input->post('input_laporan')
		);
		
		$this->db->insert('tb_laporan', $data);
	}
	
	public function view(){
		return $this->db->get('tb_laporan')->result();
	}
	
	public function edit_path($result,$id_laporan){
      $data = array(
        "path_laporan" => './uploads/'.$result['file']['file_name']
      );

      $this->db->where('id_laporan', $id_laporan);
      $this->db->update('tb_laporan', $data); // Untuk mengeksekusi perintah update data
  }
	
	
}