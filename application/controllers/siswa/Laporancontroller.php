<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporancontroller extends CI_Controller {
	
	public function __construct(){
    parent::__construct();

    $this->load->model('siswa/LaporanModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');
    $this->load->library('form_validation');

    if ($this->session->userdata('user_level')!="Siswa") {
      redirect('logincontroller');
    }

    $id_laporan = $this->session->userdata('id_laporan');
  }
  
  public function index()
  {
	$data['header'] = 'Data Laporan';
    $data['content'] = 'siswa/menu/laporan/view';
    $data['laporan'] = $this->LaporanModel->view();
    $this->load->view('siswa/home', $data);
	
  }
  
   public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->LaporanModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->LaporanModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('siswa/laporancontroller');
      }
    }
	
    $data['header'] = 'Data Laporan';
    $data['content'] = 'siswa/menu/laporan/form_tambah';
    $this->load->view('siswa/home', $data);
  }
  
public function upload_file($id_laporan)
      {

        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'doc|docx';
        $config['file_name']            = $nmfile;


        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {

			$this->form_validation->set_error_delimiters('<p class="error">', '</p>');


			$data['pesan'] = $this->upload->display_errors();
			$data['header'] = 'Data Laporan';
			$data['content'] = 'siswa/menu/laporan/view';
			$data['laporan'] = $this->LaporanModel->view($this->session->userdata('nis'));
			$this->load->view('siswa/home', $data);
         }
         else
        {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                  $this->SiswaModel->edit_path($return,$id_laporan);

                  $data['pesan'] = 'Upload Succes'; //$this->upload->data();
                  $data['header'] = 'Data Laporan';
                  $data['content'] = 'siswa/menu/laporan/view';
                  $data['laporan'] = $this->LaporanModel->view($this->session->userdata('nis'));
                  $this->load->view('siswa/home', $data);
              }

      } 	
	
	
}
	
	
  
  
  
  
  
  
  
  
  