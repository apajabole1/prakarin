<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logincontroller extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('LoginModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');
  }

  private function _validasiLogin(){
		if ( $this->session->userdata('is_login') == 1 ) {
      if ($this->session->userdata('is_superadmin')) {
        redirect(base_url('superadmin/admincontroller'));
      }elseif ($this->session->userdata('is_admin')) {
        redirect(base_url('admin/jurusancontroller'));
      }elseif ($this->session->userdata('is_siswa')) {
        redirect(base_url('siswa/siswacontroller'));
      }
		}
	}

  public function index()
	{
    $this->_validasiLogin();
	  $this->load->view('login');
	}

  public function proses_login()
  {
    $this->_validasiLogin();
    $user_email = $this->input->post('input_user_email');
    $user_password = $this->input->post('input_user_password');
    $user_level = $this->input->post('input_level');

    $ceklogin = $this->LoginModel->login($user_email,$user_password,$user_level);

    if ($user_level == "Superadmin") {
      if($ceklogin) {
        foreach ($ceklogin as $row) {
          $this->session->set_userdata('user_email', $row->user_email);
          $this->session->set_userdata('is_superadmin', 1);
          $this->session->set_userdata('is_login', 1);
          redirect('superadmin/admincontroller');
        }
      } else {
        $data['pesan']="Email dan Password salah";
        $this->load->view('login',$data);
		  }
	  } elseif ($user_level == "Admin") {
      if ($ceklogin) {
        foreach ($ceklogin as $row) {
          $this->session->set_userdata('user_email', $row->user_email);
          $this->session->set_userdata('user_verifikasi', $row->user_verifikasi);

          if ($this->session->userdata('user_verifikasi')=='1') {
            $this->session->set_userdata('is_admin', 1);
            $this->session->set_userdata('is_login', 1);
            redirect('admin/jurusancontroller');
          } elseif ($this->session->userdata('user_verifikasi')=='0') {
            $data['pesan']="Akun Anda belum diverifikasi, Harap Menghubungi Superadmin.";
            $this->load->view('login',$data);
          }
        }
      } else {
        $data['pesan']="Email dan Password admin tidak sesuai.";
        $this->load->view('login',$data);
      }
    } elseif ($user_level == "Siswa" || $user_level == "Kajur" ) {
      if ($ceklogin) {
        foreach ($ceklogin as $row) {
          $this->session->set_userdata('user_email', $row->user_email);
          $this->session->set_userdata('user_level', $row->user_level);
          $this->session->set_userdata('nis', $row->nis);
          $this->session->set_userdata('is_login', 1);
          // $this->session->set_userdata('id_pembimbing', $row->id_pembimbing);

          if(isset($row->nama_siswa)){
              $this->session->set_userdata('nama_siswa', $row->nama_siswa);
          }

          $this->session->set_userdata('hak_akses', $row->hak_akses);

          if ($user_level =='Siswa') {
            $this->session->set_userdata('is_siswa', 1);
            redirect('siswa/siswacontroller');
          } elseif ($user_level =='Kajur') {
            redirect('kajur/rekapitulasicontroller');
          } elseif ($user_level =='Kaprodi') {
            redirect('kaprodi/rekapitulasicontroller');
          } elseif ($this->session->userdata('user_level')=='Dosen') {
            redirect('dosen/rekapitulasicontroller');
          }

        }
      } else
	  {
        $data['pesan']="Email dan Password $user_level tidak sesuai.";
        $this->load->view('login',$data);
      }
    } else {
      $data['pesan']="Email dan Password Anda tidak sesuai.";
      $this->load->view('login',$data);
    }

  }


  function logout(){
    $this->session->sess_destroy();
    redirect('logincontroller');
  }
}
