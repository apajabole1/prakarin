<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusancontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/JurusanModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');
	$this->load->library('form_validation');

    if ($this->session->userdata('user_verifikasi')!="1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Jurusan';
    $data['content'] = 'admin/menu/jurusan/view';
    $data['jurusan'] = $this->JurusanModel->view();
    $this->load->view('admin/home', $data);
	}

  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->JurusanModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->JurusanModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('admin/jurusancontroller');
      }
    }
    $data['header'] = 'Data Jurusan';
    $data['content'] = 'admin/menu/jurusan/form_tambah';
    $this->load->view('admin/home', $data);
  }

  public function hapus($id_jurusan){
    $this->JurusanModel->delete($id_jurusan); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('admin/jurusancontroller');
  }

  public function ubah($id_jurusan){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->JurusanModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->JurusanModel->edit($id_jurusan); // Panggil fungsi edit() yang ada di SiswaModel.php
        redirect('admin/jurusancontroller');
      }
    }
    $data['header'] = 'Data Jurusan';
    $data['content'] = 'admin/menu/jurusan/form_ubah';
    $data['jurusan'] = $this->JurusanModel->view_by($id_jurusan);
    $this->load->view('admin/home', $data);
  }
  
}
