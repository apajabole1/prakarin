<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('superadmin/AdminModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_email')!="superadmin@gmail.com") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Admin';
    $data['content'] = 'superadmin/menu/admin/view';
    $data['admin'] = $this->AdminModel->view();
    $this->load->view('superadmin/home', $data);
	}

  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->AdminModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->AdminModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('superadmin/admincontroller');
      }
    }
    $data['header'] = 'Data Admin';
    $data['content'] = 'superadmin/menu/admin/form_tambah';
    $this->load->view('superadmin/home', $data);
  }

  public function ubah($user_id){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->AdminModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->AdminModel->edit($user_id); // Panggil fungsi edit() yang ada di SiswaModel.php
        redirect('superadmin/admincontroller');
      }
    }
    $data['header'] = 'Data Admin';
    $data['content'] = 'superadmin/menu/admin/form_ubah';
    $data['admin'] = $this->AdminModel->view_by($user_id);
    $this->load->view('superadmin/home', $data);
  }

  public function hapus($user_id){
    $this->AdminModel->delete($user_id); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('superadmin/admincontroller');
  }
}
  