<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Pembimbing</h3></center>
      </div>

	  
      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>NIP</th>
            <th>Nama Pembimbing</th>
            <th hidden colspan="2">Aksi</th>
          </tr>

          <?php
          if( ! empty($pembimbing)){
            foreach($pembimbing as $data){
              $id_pembimbing = rawurlencode($data->id_pembimbing);
              echo "<tr>
              <td>".$data->id_pembimbing."</td>
              <td>".$data->nama_pembimbing."</td>

              <td hidden ><a href='".base_url("/siswa/pembimbingcontroller/ubah/".$id_pembimbing)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td hidden ><a href='".base_url("/siswa/pembimbingcontroller/hapus/".$id_pembimbing)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Delete</button></a></td>
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
